package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.marker.IntegrationCategory;

import javax.xml.ws.WebServiceException;
import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public final class TaskDTOEndpointTest {

//    @NotNull
//    private static final EndpointLocator endpointLocator = new Bootstrap();
//
//    @NotNull
//    private static TaskEndpoint taskEndpoint;
//
//    @NotNull
//    private static SessionDTO session;
//
//    @NotNull
//    private TaskDTO task = new TaskDTO();
//
//    @BeforeClass
//    public static void initData() {
//        taskEndpoint = endpointLocator.getTaskEndpoint();
//        session = endpointLocator.getSessionEndpoint().openSession("test", "test");
//    }
//
//    @AfterClass
//    public static void closeSession() {
//        endpointLocator.getSessionEndpoint().closeSessionAll(session);
//    }
//
//    @Before
//    public void addTask() {
//        task.setName("name");
//        task.setDescription("description");
//        task.setId("123");
//        task.setUserId(session.getUserId());
//        taskEndpoint.addTask(session, task);
//    }
//
//    @After
//    public void clearData() {
//        taskEndpoint.clearAllTasks(session);
//    }
//
//    @Test
//    public void testAddAllTasks() {
//        List<TaskDTO> tasks = new ArrayList<>();
//        tasks.add(new TaskDTO());
//        tasks.add(new TaskDTO());
//        tasks.add(new TaskDTO());
//        taskEndpoint.addAllTasks(session, tasks);
//        List<TaskDTO> testTasks = taskEndpoint.finaAllTasks(session);
//        Assert.assertNotNull(testTasks);
//        Assert.assertEquals(4, testTasks.size());
//    }
//
//    @Test
//    public void testAdd() {
//        Assert.assertEquals(1, taskEndpoint.findAllTasksBySession(session).size());
//        taskEndpoint.addTask(session, task);
//        Assert.assertEquals(2, taskEndpoint.findAllTasksBySession(session).size());
//    }
//
//
//    @Test
//    public void testCreateTaskByName() {
//        taskEndpoint.createTaskByName(session, "name");
//        Assert.assertEquals(2, taskEndpoint.findAllTasksBySession(session).size());
//        TaskDTO task = taskEndpoint.findTaskOneByName(session, "name");
//        Assert.assertNotNull(task);
//        Assert.assertEquals("name", task.getName());
//    }
//
//    @Test
//    public void createTaskByNameDescription() {
//        taskEndpoint.createTaskByNameDescription(session, "name", "description");
//        TaskDTO task = taskEndpoint.findTaskOneByName(session, "name");
//        Assert.assertNotNull(task);
//        Assert.assertEquals("name", task.getName());
//        Assert.assertEquals("description", task.getDescription());
//    }
//
//    @Test
//    public void testRemoveOneById() {
//        Assert.assertEquals(1, taskEndpoint.findAllTasksBySession(session).size());
//        taskEndpoint.removeTaskOneById(session, task.getId());
//        Assert.assertEquals(0, taskEndpoint.findAllTasksBySession(session).size());
//    }
//
//    @Test
//    public void testRemoveByIndex() {
//        Assert.assertEquals(1, taskEndpoint.findAllTasksBySession(session).size());
//        taskEndpoint.removeTaskOneByIndex(session, 0);
//        Assert.assertEquals(0, taskEndpoint.findAllTasksBySession(session).size());
//    }
//
//    @Test
//    public void testRemoveByName() {
//        Assert.assertEquals(1, taskEndpoint.findAllTasksBySession(session).size());
//        taskEndpoint.removeTaskOneByName(session, "name");
//        Assert.assertEquals(0, taskEndpoint.findAllTasksBySession(session).size());
//    }
//
//    @Test
//    public void testClearTaskBySession() {
//        Assert.assertEquals(1, taskEndpoint.findAllTasksBySession(session).size());
//        taskEndpoint.clearAllTasksBySession(session);
//        Assert.assertEquals(0, taskEndpoint.findAllTasksBySession(session).size());
//    }
//
//    @Test
//    public void testClearAllTasks() {
//        Assert.assertEquals(1, taskEndpoint.findAllTasksBySession(session).size());
//        taskEndpoint.clearAllTasks(session);
//        Assert.assertEquals(0, taskEndpoint.findAllTasksBySession(session).size());
//    }
//
//    @Test
//    public void testUpdateById() {
//        TaskDTO testTask = taskEndpoint.updateTaskById(session, task.getId(), "test1", "desc");
//        Assert.assertNotNull(testTask);
//        Assert.assertEquals(task.getId(), testTask.getId());
//        Assert.assertEquals("test1", testTask.getName());
//        Assert.assertEquals("desc", testTask.getDescription());
//    }
//
//    @Test
//    public void testUpdateByIndex() {
//        TaskDTO testTask = taskEndpoint.updateTaskByIndex(session, 0, "test1", "desc");
//        Assert.assertNotNull(testTask);
//        Assert.assertEquals(task.getId(), testTask.getId());
//        Assert.assertEquals("test1", testTask.getName());
//        Assert.assertEquals("desc", testTask.getDescription());
//    }
//
//    @Test
//    public void testFindAll() {
//        taskEndpoint.addTask(session, task);
//        taskEndpoint.addTask(session, task);
//        taskEndpoint.addTask(session, task);
//        taskEndpoint.addTask(session, task);
//        taskEndpoint.addTask(session, task);
//        taskEndpoint.addTask(session, task);
//        List<TaskDTO> tasks = taskEndpoint.findAllTasksBySession(session);
//        Assert.assertNotNull(tasks);
//        Assert.assertEquals(7, tasks.size());
//    }
//
//    @Test
//    public void testFindByName() {
//        TaskDTO testTask = taskEndpoint.findTaskOneByName(session, "name");
//        Assert.assertNotNull(testTask);
//        Assert.assertEquals(task.getId(), testTask.getId());
//    }
//
//    @Test
//    public void testFindOneById() {
//        TaskDTO testTask = taskEndpoint.findTaskOneById(session, task.getId());
//        Assert.assertNotNull(testTask);
//        Assert.assertEquals(task.getId(), testTask.getId());
//    }
//
//    @Test
//    public void testFindByIndex() {
//        TaskDTO testTask = taskEndpoint.findTaskOneByIndex(session, 0);
//        Assert.assertNotNull(testTask);
//        Assert.assertEquals(task.getId(), testTask.getId());
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameWithNullSession() {
//        taskEndpoint.createTaskByName(null, "name");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameWithNullName() {
//        taskEndpoint.createTaskByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameWithEmptyName() {
//        taskEndpoint.createTaskByName(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameDescriptionWithNullSession() {
//        taskEndpoint.createTaskByNameDescription(null, "name", "description");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameDescriptionWithNullName() {
//        taskEndpoint.createTaskByNameDescription(session, null, "description");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameDescriptionWithEmptyName() {
//        taskEndpoint.createTaskByNameDescription(session, "", "description");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameDescriptionWithNullDescription() {
//        taskEndpoint.createTaskByNameDescription(session, "name", null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateTaskByNameDescriptionWithEmptyDescription() {
//        taskEndpoint.createTaskByNameDescription(session, "name", "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeCreateTaskByNameWithNullSession() {
//        taskEndpoint.clearAllTasks(null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeAddTaskWithNullSession() {
//        taskEndpoint.addTask(null, task);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeAddAllTasksWithNullSession() {
//        taskEndpoint.addAllTasks(null, new ArrayList<>());
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByNameWithNullSession() {
//        taskEndpoint.removeTaskOneByName(null, "name");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByNameWithEmptySession() {
//        taskEndpoint.removeTaskOneByName(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByNameWithNullName() {
//        taskEndpoint.removeTaskOneByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByIndexWithNullSession() {
//        taskEndpoint.removeTaskOneByIndex(null, 0);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByIndexWithNullName() {
//        taskEndpoint.removeTaskOneByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByIdWithNullSession() {
//        taskEndpoint.removeTaskOneById(null, "123");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByIdWithNullId() {
//        taskEndpoint.removeTaskOneById(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveTaskOneByIdWithEmptyId() {
//        taskEndpoint.removeTaskOneById(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindAllTasksWithNullSession() {
//        taskEndpoint.findAllTasksBySession(null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByNameWithNullSession() {
//        taskEndpoint.findTaskOneByName(null, "name");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByNameWithEmptyName() {
//        taskEndpoint.findTaskOneByName(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByNameWithNullName() {
//        taskEndpoint.findTaskOneByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByIndexWithNullSession() {
//        taskEndpoint.findTaskOneByIndex(null, 0);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByIndexWithMinusIndex() {
//        taskEndpoint.findTaskOneByIndex(null, -2);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByIdWithNullSession() {
//        taskEndpoint.findTaskOneById(null, "123");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByIdWithEmptyId() {
//        taskEndpoint.findTaskOneById(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindTaskOneByIdWithNullName() {
//        taskEndpoint.findTaskOneById(session, null);
//    }

}
