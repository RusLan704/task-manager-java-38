package ru.bakhtiyarov.tm.listener.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminUserEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.UserDTO;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "unlock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user in program.";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        UserDTO user = adminUserEndpoint.unLockUserByLogin(session, login);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
