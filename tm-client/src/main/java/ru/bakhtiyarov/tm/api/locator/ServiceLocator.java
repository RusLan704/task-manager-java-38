package ru.bakhtiyarov.tm.api.locator;

import ru.bakhtiyarov.tm.api.service.ICommandService;
import ru.bakhtiyarov.tm.api.service.ISessionService;

public interface ServiceLocator {

    ICommandService getCommandService();

    ISessionService getSessionService();

}
