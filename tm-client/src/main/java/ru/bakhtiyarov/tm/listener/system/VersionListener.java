package ru.bakhtiyarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;

@Component
public final class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

}