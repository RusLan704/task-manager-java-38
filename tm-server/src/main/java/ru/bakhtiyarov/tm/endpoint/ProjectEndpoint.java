package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.IProjectEndpoint;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.service.converter.ProjectConverter;
import ru.bakhtiyarov.tm.service.converter.SessionConverter;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
@Controller
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final SessionConverter sessionConverter;

    @NotNull
    private final ProjectConverter projectConverter;

    @NotNull
    @Autowired
    public ProjectEndpoint(
            @NotNull final IProjectService projectService,
            @NotNull final SessionConverter sessionConverter,
            @NotNull final ProjectConverter projectConverter
    ) {
        this.projectService = projectService;
        this.sessionConverter = sessionConverter;
        this.projectConverter = projectConverter;
    }

    @Override
    @SneakyThrows
    public void createProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    public void createProjectByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Override
    @SneakyThrows
    public void removeAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.findOneById(session.getUserId(), id);
        return projectConverter.toDTO(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable List<Project> projects = projectService.findAll(sessionDTO.getUserId());
        return projects
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.findOneByIndex(session.getUserId(), index);
        return projectConverter.toDTO(project);
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.findOneByName(session.getUserId(), name);
        return projectConverter.toDTO(project);
    }

    @Override
    @SneakyThrows
    public void removeProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @SneakyThrows
    public void removeProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.removeOneById(session.getUserId(), id);
    }

    @Override
    @SneakyThrows
    public void removeProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.updateProjectById(session.getUserId(), id, name, description);
        return projectConverter.toDTO(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.updateProjectByIndex(session.getUserId(), index, name, description);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

}
