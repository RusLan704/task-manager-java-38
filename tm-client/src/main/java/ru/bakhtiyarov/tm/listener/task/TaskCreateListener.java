package ru.bakhtiyarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @EventListener(condition = "@taskCreateListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER TASK NAME:");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        taskEndpoint.createTaskByNameDescription(session, projectName, taskName, description);
        System.out.println("[OK]");
    }

}
