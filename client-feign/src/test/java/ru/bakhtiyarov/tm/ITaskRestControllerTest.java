package ru.bakhtiyarov.tm;

import org.junit.Assert;
import org.junit.Test;
import ru.bakhtiyarov.tm.dto.TaskDTO;

import static ru.bakhtiyarov.tm.endpoint.ITaskRestController.client;

public class ITaskRestControllerTest {

    @Test
    public void findAllTest() {
        Assert.assertNotNull(client().findAll());
    }

    @Test
    public void createTest() {
        final TaskDTO task = new TaskDTO();
        task.setName("123");
        task.setDescription("435");
        client().create(task);
        TaskDTO tempProject = client().findById(task.getId());
        Assert.assertEquals(tempProject.getName(), "123");
        Assert.assertEquals(tempProject.getDescription(), "435");
        client().removeOneById(task.getId());
    }

    @Test
    public void removeTest() {
        final TaskDTO task = new TaskDTO();
        task.setName("123");
        task.setDescription("123");
        client().create(task);
        client().removeOneById(task.getId());
        Assert.assertFalse(client().findAll().contains(task));
    }

    @Test
    public void updateTest() {
        final TaskDTO task = new TaskDTO();
        task.setName("123");
        task.setDescription("345");
        client().create(task);
        task.setName("111");
        task.setDescription("000");
        client().updateTaskById(task);
        TaskDTO taskUpdated = client().findById(task.getId());
        Assert.assertEquals(taskUpdated.getName(), "111");
        Assert.assertEquals(taskUpdated.getDescription(), "000");
        client().removeOneById(task.getId());
    }

}
