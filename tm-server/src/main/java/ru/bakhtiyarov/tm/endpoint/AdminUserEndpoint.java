package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.IAdminUserEndpoint;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.api.service.converter.ISessionConverter;
import ru.bakhtiyarov.tm.api.service.converter.IUserConverter;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
@Controller
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISessionConverter sessionConverter;

    @NotNull
    private final IUserConverter userConverter;

    @NotNull
    @Autowired
    public AdminUserEndpoint(
            @NotNull final IUserService userService,
            @NotNull final ISessionConverter sessionConverter,
            @NotNull final IUserConverter userConverter
    ) {
        this.userService = userService;
        this.sessionConverter = sessionConverter;
        this.userConverter = userConverter;
    }

    @Override
    public @Nullable UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        @Nullable User user = userService.lockUserByLogin(login);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Override
    public @Nullable UserDTO unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        @Nullable User user = userService.unLockUserByLogin(login);
        @Nullable UserDTO userDTO = userConverter.toDTO(user);
        return userDTO;
    }

    @Nullable
    @Override
    public void removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Nullable
    @Override
    public void removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {

        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAllUsers(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable final Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session, Role.ADMIN);
        @Nullable List<User> user = userService.findAll();
        return user
                .stream()
                .map(userConverter::toDTO)
                .collect(Collectors.toList());
    }

}
