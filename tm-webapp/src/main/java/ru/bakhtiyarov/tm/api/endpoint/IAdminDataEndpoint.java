package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.SessionDTO;

import javax.jws.WebParam;

public interface IAdminDataEndpoint {

    void loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void clearJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void clearYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void loadYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

    void saveYaml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception;

}
