package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    @NotNull
    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}