package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    @NotNull
    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
