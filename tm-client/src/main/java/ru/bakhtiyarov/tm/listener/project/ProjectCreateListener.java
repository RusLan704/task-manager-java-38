package ru.bakhtiyarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @EventListener(condition = "@projectCreateListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        projectEndpoint.createProjectByNameDescription(session, name, description);
        System.out.println("[OK]");
    }

}
